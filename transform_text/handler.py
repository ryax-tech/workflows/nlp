#!/usr/bin/env python3
#Copyright (c) Ryax Technologies

import os
import pickle
import datetime


def save_object(obj, where):
    with open(where, 'wb') as f:
        pickle.dump(obj, f, pickle.HIGHEST_PROTOCOL)

def load_object(where):
    with open(where, 'rb') as f:
        return pickle.load(f)

def handle_string_for_nlp(s):
    return [
            {
                "timestamp": str(datetime.datetime.now()),
                "text": s,
            },
    ]

def handle(req):
    in_string = req.get("in_string")
    print(f"Transforming text: {in_string}")
    ret = handle_string_for_nlp(in_string)
    save_location = '/tmp/transformed_data.pkl'
    save_object(ret, save_location)
    print(f"Saved result in: {save_location}\nDone!")
    return {'data_path' : save_location}

if __name__=='__main__':
    raw_in = input("Give me a string as input\n>>> ")
    in_dict = {'in_string': raw_in}
    print(f"Running module locally with input: {in_dict}")
    out = handle(in_dict)
    print(f"Output: {out}")
    file_data = load_object(out.get('data_path'))
    print(f"Transformed data: {file_data}")
    print("Deleting output file")
    os.remove(out.get('data_path'))
    print("Done")

    
    



