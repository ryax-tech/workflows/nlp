import json
import torch
import pickle
import os

CACHEDIR = '/tmp/cache'
if not os.path.isdir(CACHEDIR):
    os.mkdir(CACHEDIR)
os.environ['TRANSFORMERS_CACHE'] = '/tmp/cache'

from transformers import pipeline

MODEL_PATH = '/tmp/distilbert_text_classif/'
if not os.path.isdir(MODEL_PATH):
    os.mkdir(MODEL_PATH)

DISTILBERT_SEQ_CLASSIF_FILES = [
        'tokenizer_config.json',
        'special_tokens_map.json',
        'config.json',
        'modelcard.json',
        'vocab.txt',
        'pytorch_model.bin'
        ]

def save_object(d, where):
    with open(where, 'wb') as f:
        pickle.dump(d, f, pickle.HIGHEST_PROTOCOL)

def load_object(where):
    with open(where, 'rb') as f:
        return pickle.load(f)

def transformer_model_is_properly_config(d, model_files):
    if os.path.isdir(d):
        if all(x in os.listdir(d) for x in model_files):
            return True
    else:
        os.makedirs(d)
    return False

def handle(req):
    data_file = req.get('data_path')
    data = load_object( data_file )
    print(f"Got data from {data_file}")

    text_only = [x.get('text') for x in data]
    
    if not transformer_model_is_properly_config(
            MODEL_PATH,
            DISTILBERT_SEQ_CLASSIF_FILES
            ):
        print("Downloading pretrained model")
        pipe = pipeline("sentiment-analysis")
        pipe.save_pretrained(MODEL_PATH)
    else:
        print("Loading cached model")
        pipe = pipeline("sentiment-analysis", model = MODEL_PATH) 
    
    print("Processing text...")
    predictions = pipe(text_only)
    
    for i, item in enumerate(predictions):
        data[i].update(
                prediction=item.get('label'),
                confidence=item.get('score')
        )

    print(f"Finished processing. Result:\n{json.dumps(data,indent=4)}") 
    print(f"Saving data in {data_file}")
    save_object(data, data_file)
    print("Done!")
    

    return {'predictions': data_file}

if __name__ == "__main__":
    data = [
            {"text": "Despite setbacks, the result was overall positive"},
            {"text": "Today was a terrible day"},
            {"text": "Pleased to meet you!"},
            ]
    loc = '/tmp/test.pkl'
    save_object(data, loc)
    res = handle( {'data_path': loc} )
    result_file = res.get('predictions') 
    os.remove(loc)
    for item in DISTILBERT_SEQ_CLASSIF_FILES:
        os.remove(MODEL_PATH+item)
        print(f"Removed {MODEL_PATH+item}")
    os.rmdir(MODEL_PATH)
    print(f"Removed {MODEL_PATH}")
    print(f"Model information cached in {CACHEDIR}")
    print("Done!")


