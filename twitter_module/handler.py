#!/usr/bin/env python3
# Copyright (c) Ryax Technologies

import tweepy
import json
import pickle

CALLBACK_URL = "twitter.com"

class TwitterAPI:
    def __init__(
        self,
        api_key: str = None,
        api_secret_key: str = None,
        access_key: str = None,
        access_secret: str = None,
    ):
        self.api_key = api_key
        self.api_secret_key = api_secret_key
        self.access_key = access_key
        self.access_secret = access_secret

    def get_auth(self):
        self.auth = tweepy.OAuthHandler(self.api_key, self.api_secret_key, CALLBACK_URL)

    def set_access(self):
        self.auth.set_access_token(self.access_key, self.access_secret)

    def get_api(self):
        self.get_auth()
        self.set_access()
        return tweepy.API(self.auth)


class RyaxTweetCrawler:
    def __init__(self, api=None):
        self.api = api

    def n_tweets_from_query(self, n, query):
        data = []
        for tweet in tweepy.Cursor(self.api.search, q=query, tweet_mode= 'extended').items(n):
            if hasattr(tweet,'retweeted_status'):
                full_tweet = str(tweet.retweeted_status.full_text)
            else:
                full_tweet = str(tweet.full_text)

            data.append(
                {
                    "timestamp": str(tweet.created_at),
                    "user": str(tweet.user.screen_name),
                    "text": full_tweet,
                }
            )
        return data

def json_from_file(path):
    with open(path, "r") as f:
        ret = json.load(f)
    return ret

def save_object(obj, where):
    with open(where, 'wb') as f:
        pickle.dump(obj, f, pickle.HIGHEST_PROTOCOL)

def load_object(where):
    with open(where, 'rb') as f:
        return pickle.load(f)

    
def handle(req):
    try: 
        num_tweets = req.get("num_tweets")
        query = req.get("query")

        api_key = req.get("api_key")
        api_secret = req.get("api_secret_key")
        access_key = req.get("access_key")
        access_secret = req.get("access_secret")
        print("Got credentials\nConnection to twitter api...")
        api = TwitterAPI(
                api_key = api_key,
                api_secret_key = api_secret,
                access_key = access_key,
                access_secret = access_secret
                ).get_api()

        crawler = RyaxTweetCrawler(api=api)
        print("Connected!\nProcessing Tweets...")

        data = crawler.n_tweets_from_query(num_tweets, query)
        
        save_location = '/tmp/tweet_data2.pkl'
        save_object(data, save_location)
        print(f"Done, data saved in {save_location}")

        return {'data_path' : save_location}

    except Exception as e:
        print(e)


if __name__ == "__main__":
    inputs = {
        'num_tweets': 2000,
        'query': 'Programming'
        }
    creds = json_from_file('./charles_credentials.json' )
    inputs.update(creds)

    res = handle(inputs)
    print(f"Inputs:\t{inputs}\n")
    print(f"Result Location: {res}\n")
    
    #data = load_object(res.get('data_path'))
    #print(f"Contents:\t{data}\n")
    exit()
    import os
    os.remove(res.get('data_path'))
    print("Result file removed.\n\nDone")

